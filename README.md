# PHYSLITE python demo (uproot/awkward/vector)
[![](https://swanserver.web.cern.ch/swanserver/images/badge_swan_white_150.png)](https://cern.ch/swanserver/cgi-bin/go?projurl=https://gitlab.cern.ch/nihartma/end-of-year-physlite-tutorial.git)

Shown at the [End-of-the year physics plenary 2023](https://indico.cern.ch/event/1335237)

Click [Open in SWAN](https://cern.ch/swanserver/cgi-bin/go?projurl=https://gitlab.cern.ch/nihartma/end-of-year-physlite-tutorial.git) to follow along. Use the default settings - **Software stack:** 104a, **Platform:** CentOS 7 (gcc11)

To run locally, install the packages from `requirements.txt`

